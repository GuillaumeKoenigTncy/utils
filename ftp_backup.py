from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
import smtplib
import ssl
import glob
import paramiko

FTP_ADDR = ""
FTP_USER = ""
FTP_PAWD = ""

MAIL_FROM = ""
MAIL_TO = ""
MAIL_USER = ""
MAIL_PAWD = ""

if __name__ == "__main__":

    list_of_files = glob.glob(r'/home/backup/2023*')
    latest_file = max(list_of_files, key=os.path.getctime)

    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(FTP_ADDR, username=FTP_USER, password=FTP_PAWD)

        sftp = ssh.open_sftp()
        sftp.put(latest_file, latest_file.replace("/home/backup/", ""))
        sftp.close()
        ssh.close()

        msg = MIMEMultipart('alternative')
        msg['From'] = MAIL_FROM
        msg['To'] = MAIL_TO
        msg['Subject'] = "Backup"

        mimed_plain = MIMEText(f"Le backup {latest_file.replace('/home/backup/', '')} a été téléversé correctement.", 'plain')

        msg.attach(mimed_plain)

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as server:
            server.login(MAIL_FROM, MAIL_PAWD)
            server.sendmail(MAIL_FROM, MAIL_TO, msg.as_string())
            server.quit()

    except Exception as e:

        print(e)

        msg = MIMEMultipart('alternative')
        msg['From'] = MAIL_FROM
        msg['To'] = MAIL_TO
        msg['Subject'] = "Backup"

        mimed_plain = MIMEText("Une erreur est survenue lors du téléversement de la sauvegarde quotidienne !", 'plain')

        msg.attach(mimed_plain)

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as server:
            server.login(MAIL_FROM, MAIL_PAWD)
            server.sendmail(MAIL_FROM, MAIL_TO, msg.as_string())
            server.quit()
