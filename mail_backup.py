from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
import smtplib
import ssl
import os
import gzip
import glob

FTP_ADDR = ""
FTP_USER = ""
FTP_PAWD = ""

MAIL_FROM = ""
MAIL_TO = ""
MAIL_USER = ""
MAIL_PAWD = ""

if __name__ == "__main__":

    list_of_files = glob.glob(r'/home/backup/2023*')
    latest_file = max(list_of_files, key=os.path.getctime)

    msg = MIMEMultipart('alternative')
    msg['From'] = MAIL_FROM
    msg['To'] = MAIL_TO
    msg['Subject'] = "Backup"

    mimed_html = MIMEText("resr", 'html')
    mimed_plain = MIMEText("resr", 'plain')

    msg.attach(mimed_plain)
    msg.attach(mimed_html)

    filename = latest_file

    f = gzip.open(latest_file,'rb')

    attachment = MIMEApplication(f.read())
    attachment.add_header('Content-Disposition', 'attachment',   filename=filename.replace("/home/backup/", "").replace(".db.sql.gz", ".db.sql"))
    msg.attach(attachment)

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as server:
        server.login(MAIL_FROM, MAIL_PAWD)
        server.sendmail(MAIL_FROM, MAIL_TO, msg.as_string())
        server.quit()
